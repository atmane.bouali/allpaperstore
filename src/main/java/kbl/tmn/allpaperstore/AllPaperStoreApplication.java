package kbl.tmn.allpaperstore;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import kbl.tmn.allpaperstore.model.User;
import kbl.tmn.allpaperstore.model.security.Role;
import kbl.tmn.allpaperstore.model.security.UserRole;
import kbl.tmn.allpaperstore.service.UserService;
import kbl.tmn.allpaperstore.util.SecurityUtility;

@SpringBootApplication
public class AllPaperStoreApplication implements CommandLineRunner{

	@Autowired
	UserService userService;
	
	public static void main(String[] args) {
		SpringApplication.run(AllPaperStoreApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		User user = new User("atmane", SecurityUtility.passwordEncoder().encode("atmane"), "atmane", "bouali", "katia.aouchar06@gmail.com", "0664419685", "2 impasse des orteaux 75020 Paris", "DevMan");
		
		Role role = new Role(new Long(1), "ROLE_USER");
		Set<UserRole> userRoles = new HashSet<>();
		userRoles.add(new UserRole(user, role));
		
		userService.createUser(user, userRoles);
	}

}
