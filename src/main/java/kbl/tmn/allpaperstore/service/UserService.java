package kbl.tmn.allpaperstore.service;

import java.util.Set;

import kbl.tmn.allpaperstore.model.User;
import kbl.tmn.allpaperstore.model.security.PasswordResetToken;
import kbl.tmn.allpaperstore.model.security.UserRole;

public interface UserService {
	PasswordResetToken getPasswordRestToken(final String token);
	
	void createPasswordResetTokenForUser(final User user, final String token);
	
	User findById(Long id);
	User findByUsername(String username);
	User findByEmail(String email);
	Long countByUsername(String username);
	Long countByEmail(String email);
	User createUser(User user, Set<UserRole> userRoles) throws Exception;
	User updateUser(User user);
}
