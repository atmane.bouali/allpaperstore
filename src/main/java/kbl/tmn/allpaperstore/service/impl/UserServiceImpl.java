package kbl.tmn.allpaperstore.service.impl;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kbl.tmn.allpaperstore.model.User;
import kbl.tmn.allpaperstore.model.security.PasswordResetToken;
import kbl.tmn.allpaperstore.model.security.UserRole;
import kbl.tmn.allpaperstore.repository.PasswordResetTokenRepository;
import kbl.tmn.allpaperstore.repository.RoleRepository;
import kbl.tmn.allpaperstore.repository.UserRepository;
import kbl.tmn.allpaperstore.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

	
	@Autowired
	PasswordResetTokenRepository passwordResetTokenRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Override
	public PasswordResetToken getPasswordRestToken(String token) {
		return passwordResetTokenRepository.findByToken(token);
	}

	@Override
	public void createPasswordResetTokenForUser(User user, String token) {
		PasswordResetToken myToken = new PasswordResetToken(user, token);
		passwordResetTokenRepository.save(myToken);
		
	}

	@Override
	public User findById(Long id) {
		return userRepository.findById(id).get();
	}
	
	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	
	@Override
	public Long countByUsername(String username) {
		return userRepository.countByUsername(username);
	}

	@Override
	public Long countByEmail(String email) {
		return userRepository.countByEmail(email);
	}

	@Override
	public User createUser(User user, Set<UserRole> userRoles) throws Exception{
		User localUser = userRepository.findByUsername(user.getUsername());
		
		if(localUser != null) {
			// throw new Exception("L'utilisateur existe deja.");
			LOG.info("User {} already exists. Nothing will be done.", user.getUsername());
		}else {
			for (UserRole userRole : userRoles) {
				roleRepository.save(userRole.getRole()); 
			}
			
			user.getUserRoles().addAll(userRoles);
			localUser = userRepository.save(user);
		}
		return localUser;
	}
	
	@Override
	public User updateUser(User user){
		return userRepository.save(user);
	}

	
}
