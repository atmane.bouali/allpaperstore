package kbl.tmn.allpaperstore.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import kbl.tmn.allpaperstore.model.User;
import kbl.tmn.allpaperstore.repository.UserRepository;

@Service
public class UserSecurityService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
		User user = userRepository.findByUsername(username);
		if(user == null) {
			System.out.println("User name : not found :( => "+username);
			
			throw new UsernameNotFoundException("Username not found");
		}
		System.out.println("User name "+user.getFirstname()+" : "+user.getPassword() );
		
		return user;
	}
}
