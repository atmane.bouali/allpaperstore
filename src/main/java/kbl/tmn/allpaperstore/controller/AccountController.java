package kbl.tmn.allpaperstore.controller;


import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kbl.tmn.allpaperstore.model.User;
import kbl.tmn.allpaperstore.model.security.PasswordResetToken;
import kbl.tmn.allpaperstore.model.security.Role;
import kbl.tmn.allpaperstore.model.security.UserRole;
import kbl.tmn.allpaperstore.service.UserService;
import kbl.tmn.allpaperstore.service.impl.UserSecurityService;
import kbl.tmn.allpaperstore.util.MailConstructor;
import kbl.tmn.allpaperstore.util.SecurityUtility;

@Controller
public class AccountController {
	
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	MailConstructor mailConstructor;
	
	
	@Autowired
	UserService userService;
	
	@Autowired 
	UserSecurityService userSecurityService;
	
	@RequestMapping("/createAccount")
	public String createAccount(Model model) {
		model.addAttribute("classActiveCreateAccount", true);
		model.addAttribute("user", new User());
		return "myAccount";
	}
	
	@RequestMapping(value = "/createAccount", method = RequestMethod.POST)
	public String createAccount(
			HttpServletRequest request,
			@ModelAttribute("password")String password,
			@ModelAttribute("confirmPassword")String confirmPassword,
			@ModelAttribute("user")User user,
			Model model
			) throws Exception{
		model.addAttribute("classActiveCreateAccount", true);
		
		boolean emailOrUsernameExists = false;
		
		if(userService.findByUsername(user.getUsername()) != null) {
			model.addAttribute("usernameExists", true);
			emailOrUsernameExists = true;
		}
		
		if(userService.findByEmail(user.getEmail()) != null) {
			model.addAttribute("emailExists", true);
			emailOrUsernameExists = true;
		}
		
		if(!password.equals(confirmPassword)) {
			model.addAttribute("passwordsNotEqual", true);
			emailOrUsernameExists = true;
		}
		
		if(emailOrUsernameExists) {
			model.addAttribute("classActiveCreateAccount", true);
			return "myAccount";
		}
		
		String encryptedPassword = SecurityUtility.passwordEncoder().encode(password);
		user.setPassword(encryptedPassword);
		user.initializeDefautlDatesAndBooleans();
		Role role = new Role(new Long(1), "ROLE_USER");
		Set<UserRole> userRoles = new HashSet<>();
		userRoles.add(new UserRole(user, role));
		userService.createUser(user, userRoles);
		
		String token = UUID.randomUUID().toString();
		userService.createPasswordResetTokenForUser(user, token);
		
		String appUrl = "http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath();
		SimpleMailMessage simpleMailMessage = mailConstructor.constructResetTokenEmail(appUrl, request.getLocale(), token, user);
		mailSender.send(simpleMailMessage);
		model.addAttribute("classActiveCreateAccount", true);
		model.addAttribute("emailSent", true);
		
		return "myAccount";
	}
	
	@RequestMapping("/confirmCreatedAccount")
	public String confirmCreatedAccount(
			@RequestParam("token")String token,
			Model model
			) {
		
		PasswordResetToken passToken = userService.getPasswordRestToken(token);
		if(passToken == null) {
			String message = "Token non valide.";
			model.addAttribute("message", message);
			return "redirect:/badRequest";
		}
		User user = passToken.getUser();
		String username = user.getUsername();
		
		UserDetails userDetails = userSecurityService.loadUserByUsername(username);
		Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		model.addAttribute("user", user);
		model.addAttribute("classActiveConfirmCreatedAccount", true);
		model.addAttribute("confirmAccount", true);

		return "myAccount";
	}
	
	@RequestMapping("/login")
	public String login(Model model) {
		model.addAttribute("classActiveLogin", true);
		return "myAccount";
	}
	
	/*@RequestMapping("/auth")
	public String auth(Model model) {
		model.addAttribute("classActiveLogin", true);
		return "myAccount";
	}*/
	
	/*@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(Model model, HttpServletRequest request) {
		model.addAttribute("classActiveLogin", true);
		return "myAccount";
	}*/
	
	@RequestMapping("/forgetPassword")
	public String forgetPassword(Model model) {
		model.addAttribute("classActiveForgetPassword", true);
		return "myAccount";
	}
	
	@RequestMapping(value = "/forgetPassword", method = RequestMethod.POST)
	public String forgetPassword(
			HttpServletRequest request, 
			@ModelAttribute("email")String email,
			Model model
			) {
			
		User user = userService.findByEmail(email);
		
		if(user != null) {
			String rondomPassword = SecurityUtility.randomPasswString();
			String encrypedPassword = SecurityUtility.passwordEncoder().encode(rondomPassword);
			user.setPassword(encrypedPassword);
			userService.updateUser(user);
			
			SimpleMailMessage restPasswordEmail = mailConstructor.constructForgetPasswordEmail(user, rondomPassword);
			mailSender.send(restPasswordEmail);

			model.addAttribute("user", user);
			model.addAttribute("forgetPasswordEmailSent", true);
			model.addAttribute("classActiveForgetPassword", true);
			return "myAccount";
		}
		
		System.out.println("sysout.../forgetPassword");
		model.addAttribute("user", user);
		model.addAttribute("emailNotExists",true);
		model.addAttribute("classActiveForgetPassword", true);
		
		return "myAccount";
	}
	
	
}