package kbl.tmn.allpaperstore.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kbl.tmn.allpaperstore.model.User;
import kbl.tmn.allpaperstore.model.security.PasswordResetToken;
import kbl.tmn.allpaperstore.model.security.Role;
import kbl.tmn.allpaperstore.model.security.UserRole;
import kbl.tmn.allpaperstore.service.UserService;
import kbl.tmn.allpaperstore.service.impl.UserSecurityService;
import kbl.tmn.allpaperstore.util.MailConstructor;
import kbl.tmn.allpaperstore.util.SecurityUtility;

@Controller
public class ProfileController {
	
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	MailConstructor mailConstructor;
	
	@Autowired
	UserService userService;
	
	@Autowired 
	UserSecurityService userSecurityService;
	
	@RequestMapping("/myAccount")
	public String myAccount(Model model) {
		User user = userService.findByEmail("katia.aouchar06@gmail.com");
		model.addAttribute("user", user);
		model.addAttribute("classActiveViewMyProfile", true);
		return "myProfile";
	}
	
	@RequestMapping("/myAccount/view")
	public String viewMyAccount(Model model) {
		User user = userService.findByEmail("katia.aouchar06@gmail.com");
		model.addAttribute("user", user);
		model.addAttribute("classActiveViewMyProfile", true);
		return "myProfile";
	}
	
	@RequestMapping("/myAccount/edit")
	public String editMyAccount(Model model) {
		model.addAttribute("classActiveEditMyAccount", true);
		User user = userService.findByEmail("katia.aouchar06@gmail.com");
		model.addAttribute("user", user);
		return "myProfile";
	}
	
	@RequestMapping(value = "/myAccount/edit", method = RequestMethod.POST)
	public String editMyAccount(
			Model model, 
			HttpServletRequest request,
			@ModelAttribute("user") User user
			) {
		User userFromData = userService.findById(new Long(user.getId() ) );
		
		boolean emailOrUsernameExists = false;
		
		if(userService.countByEmail(user.getUsername()) > 1) {
			model.addAttribute("usernameExists", true);
			emailOrUsernameExists = true;
		}
		
		if(userService.countByEmail(user.getEmail()) > 1) {
			model.addAttribute("emailExists", true);
			emailOrUsernameExists = true;
		}
		
		if(emailOrUsernameExists) {
			model.addAttribute("user", user);
			model.addAttribute("classActiveEditMyAccount", true);
			return "myProfile";
		}
		
		userFromData.setCompany(user.getCompany());
		userFromData.setFirstname(user.getFirstname());
		userFromData.setLastname(user.getLastname());
		userFromData.setTel(user.getTel());
		userFromData.setEmail(user.getEmail());
		userFromData.setAddress(user.getAddress());
		userFromData.setUsername(user.getUsername());
		userFromData.setUpdatedAt(new Date());
		System.out.println(userFromData.getId());
		userService.updateUser(userFromData);
		model.addAttribute("updateUserInfo", true);
		model.addAttribute("classActiveEditMyAccount", true);
		return "myProfile";
	}
	
	@RequestMapping("/myAccount/changePassword")
	public String changePassword(Model model) {
		model.addAttribute("classActiveChanceMyPassword", true);
		User user = userService.findByEmail("katia.aouchar06@gmail.com");
		Long id = user.getId();
		model.addAttribute("id", id);
		return "myProfile";
	}
	
	@RequestMapping(value = "/myAccount/changePassword", method = RequestMethod.POST)
	public String changePassword(
			Model model, 
			HttpServletRequest request,
			@ModelAttribute("id") String id,
			@ModelAttribute("currentPassword") String currentPassword,
			@ModelAttribute("password") String password,
			@ModelAttribute("confirmPassword") String confirmPassword
			) {
		
		boolean passwordIsNotCorrect = false;
		
		User user = userService.findById(new Long(id));
		
		String encryptedCurrentPassword = SecurityUtility.passwordEncoder().encode(currentPassword);
		
		if( !user.getPassword().equals(encryptedCurrentPassword)) {
			model.addAttribute("currentPasswordNotCorrect", true);
			passwordIsNotCorrect = true;
		}
		
		if(!password.equals(confirmPassword)) {
			model.addAttribute("passwordsNotEqual", true);
			passwordIsNotCorrect = true;
		}
		
		if(passwordIsNotCorrect) {
			model.addAttribute("classActiveChanceMyPassword", true);
			return "myProfile";
		}
		
		String encryptedNewPassword = SecurityUtility.passwordEncoder().encode(password);
		user.setPassword(encryptedNewPassword);
		userService.updateUser(user);
		model.addAttribute("classActiveChanceMyPassword", true);
		model.addAttribute("UserPasswordUpdated", true);
		
		return "myProfile";
	}
}
