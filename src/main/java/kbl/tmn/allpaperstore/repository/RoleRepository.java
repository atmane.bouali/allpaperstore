package kbl.tmn.allpaperstore.repository;

import org.springframework.data.repository.CrudRepository;

import kbl.tmn.allpaperstore.model.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long>{
	Role findByName(String name);
}
