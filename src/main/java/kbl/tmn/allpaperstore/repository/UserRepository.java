package kbl.tmn.allpaperstore.repository;

import org.springframework.data.repository.CrudRepository;

import kbl.tmn.allpaperstore.model.User;

public interface UserRepository extends CrudRepository<User, Long>{
	User findByUsername(String username);
	User findByEmail(String email);
	Long countByUsername(String username);
	Long countByEmail(String email);
	
}

