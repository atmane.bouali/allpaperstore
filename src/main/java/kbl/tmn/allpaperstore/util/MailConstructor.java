package kbl.tmn.allpaperstore.util;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import kbl.tmn.allpaperstore.model.User;

@Component
public class MailConstructor {
	
	@Autowired
	Environment env;
	
	public SimpleMailMessage constructResetTokenEmail(String contextPath, Locale locale, String token, User user) {
		String url = contextPath + "/confirmCreatedAccount?token="+token;
		String message = "Bonjour";
		message += "\nVotre Inscription est prise en compte.";
		message += "\nAfin d'activer votre compte, veuillez cliquer sur le lien suivant : \n";
		message += url;
		message += "\n\n\nEquipe AllPapperStore.";
		SimpleMailMessage email = new SimpleMailMessage(); 
		email.setTo(user.getEmail());
		email.setSubject("AppPaperStore : Valider la création de votre compte");
		email.setText(message);
		email.setFrom(env.getProperty("support.email"));
		return email;
	}
	
	public SimpleMailMessage constructForgetPasswordEmail(User user, String password) {
		String message = "Bonjour";
		message += "\nVous venez de faire une demande de réinitialisation de votre mot de passe.";
		message += "\nPour changer votre mot mot de passe, connectez-vous sur votre compte avec ce mot de passe temporaire : \n";
		message += "Mot de passe: "+password;
		message += "\nUne fois connecté, allez sur 'Mon compte', enuite sur 'Changer mon mot de passe' pour changer votre mot de passe";
		message += "\n\n\nEquipe AllPapperStore.";
		SimpleMailMessage email = new SimpleMailMessage(); 
		email.setTo(user.getEmail());
		email.setSubject("AppPaperStore : Réinitialiser votre mot de passe");
		email.setText(message);
		email.setFrom(env.getProperty("support.email"));
		return email;
	}
}
