package kbl.tmn.allpaperstore.model.security;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import kbl.tmn.allpaperstore.model.User;

@Entity
public class PasswordResetToken {
	
	private static final int EXPRIRATION = 60 * 24;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String token;
	
	@OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
	@JoinColumn(nullable = false, name = "user_id")
	private User user;
	
	private Date expiryDate;
	
	public PasswordResetToken() {
	}
	
	public PasswordResetToken(User user, String token) {
		super();
		this.user = user;
		this.token = token;
		this.expiryDate = calculateExpiryDate(EXPRIRATION);
	}

	private Date calculateExpiryDate(final int expiryTimeInMinutes) {
		LocalDateTime currentDateTime = LocalDateTime.now();
		currentDateTime.plusMinutes(expiryTimeInMinutes);
		return  Date.from(currentDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}
	
	public void updateToken(final String token) {
		this.token = token;
		this.expiryDate = calculateExpiryDate(EXPRIRATION);
	}

	public PasswordResetToken(Long id, String token, User user, Date expriryDate) {
		super();
		this.id = id;
		this.token = token;
		this.user = user;
		this.expiryDate = expriryDate;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public static int getExpriration() {
		return EXPRIRATION;
	}

	@Override
	public String toString() {
		return "PasswordRestToken [id=" + id + ", token=" + token + ", user=" + user + ", expiryDate=" + expiryDate
				+ "]";
	}
	
}
